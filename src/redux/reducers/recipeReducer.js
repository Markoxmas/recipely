import CONST from "../constants";

const initialState = {
  recipeId: undefined,
  history: [],
};

export default function recipeReducer(state = initialState, action = {}) {
  switch (action.type) {
    case CONST.SET_RECIPE_ID:
      return {
        ...state,
        recipeId: action.id,
        history: [...state.history, action.id],
      };
    default:
      return state;
  }
}
