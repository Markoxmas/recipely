import CONST from "../constants";

const initialState = {
  similarIds: [],
  pending: false,
  error: "",
};

export default function similarRecipesReducer(
  state = initialState,
  action = {}
) {
  switch (action.type) {
    case CONST.FETCH_SIMILAR_RECIPES_PENDING:
      return { ...state, similarIds: [], pending: true, error: "" };
    case CONST.FETCH_SIMILAR_RECIPES_SUCCESS:
      return {
        ...state,
        similarIds: action.recipeIds,
        pending: false,
        error: "",
      };
    case CONST.FETCH_SIMILAR_RECIPES_FAILED:
      return { ...state, similarIds: [], pending: false, error: action.error };
    default:
      return state;
  }
}
