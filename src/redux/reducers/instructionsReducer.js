import CONST from "../constants";

const initialState = {
  instructions: [],
  pending: false,
  error: "",
};

export default function instructionsReducer(state = initialState, action = {}) {
  switch (action.type) {
    case CONST.FETCH_INSTRUCTIONS_PENDING:
      return { ...state, pending: true, instructions: [], error: "" };
    case CONST.FETCH_INSTRUCTIONS_SUCCESS:
      return {
        ...state,
        pending: false,
        instructions: action.instructions,
        error: "",
      };
    case CONST.FETCH_INSTRUCTIONS_FAILED:
      return {
        ...state,
        pending: false,
        instructions: [],
        error: action.error,
      };
    default:
      return state;
  }
}
