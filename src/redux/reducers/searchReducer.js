import CONST from "../constants";

const initialState = {
  searchField: "",
  pending: false,
  suggestions: [],
  error: "",
};

export default function searchReducer(state = initialState, action = {}) {
  switch (action.type) {
    case CONST.UPDATE_SEARCH_FIELD:
      return {
        ...state,
        searchField: action.value,
      };
    case CONST.FETCH_RECIPES_AUTO_PENDING:
      return { ...state, pending: true, suggestions: [], error: "" };
    case CONST.FETCH_RECIPES_AUTO_SUCCESS:
      return {
        ...state,
        pending: false,
        suggestions: action.suggestions,
        error: "",
      };
    case CONST.FETCH_RECIPES_AUTO_FAILED:
      return { ...state, pending: false, error: action.error, suggestions: [] };
    case CONST.FETCH_INSTRUCTIONS_PENDING:
    case CONST.FETCH_RECIPE_PENDING:
      return { ...state, pending: false, suggestions: [], searchField: "" };
    default:
      return state;
  }
}
