import CONST from "../constants";

const initialState = {
  recipes: [],
  pending: false,
  error: "",
};

export default function recipeStorageReducer(
  state = initialState,
  action = {}
) {
  switch (action.type) {
    case CONST.FETCH_RECIPES_PENDING:
      return { ...state, pending: true, error: "" };
    case CONST.FETCH_RECIPES_SUCCESS:
      return {
        ...state,
        recipes: [...state.recipes, ...action.recipes],
        pending: false,
        error: "",
      };
    case CONST.FETCH_RECIPES_FAILED:
      return { ...state, pending: false, error: action.error };
    default:
      return state;
  }
}
