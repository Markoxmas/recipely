import CONST from "../constants";
import axios from "axios";

export const onUpdateSearchField = (event) => ({
  type: CONST.UPDATE_SEARCH_FIELD,
  value: event.target.value,
});

export const fetchRecipesAutocomplete = (searchField) => (dispatch) => {
  dispatch({ type: CONST.FETCH_RECIPES_AUTO_PENDING });
  axios
    .get(
      `https://api.spoonacular.com/recipes/autocomplete?number=${5}&query=${searchField}&apiKey=${
        CONST.API_KEY
      }`
    )
    .then((response) => {
      dispatch({
        type: CONST.FETCH_RECIPES_AUTO_SUCCESS,
        suggestions: response.data,
      });
    })
    .catch((error) => {
      dispatch({
        type: CONST.FETCH_RECIPES_AUTO_FAILED,
        error: error.message,
      });
    });
};
