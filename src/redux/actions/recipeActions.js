import CONST from "../constants";
import axios from "axios";
import extractMissingIds from "../../lib/extractMissingIds";

export const onSetRecipeId = (id) => ({
  type: CONST.SET_RECIPE_ID,
  id,
});

export const fetchRecipes = (ids) => (dispatch) => {
  dispatch({ type: CONST.FETCH_RECIPES_PENDING });
  axios
    .get(
      `https://api.spoonacular.com/recipes/informationBulk?ids=${ids}&apiKey=${CONST.API_KEY}`
    )
    .then((response) => {
      dispatch({ type: CONST.FETCH_RECIPES_SUCCESS, recipes: response.data });
    })
    .catch((error) => {
      dispatch({ type: CONST.FETCH_RECIPES_FAILED, error: error.message });
    });
};

export const fetchRecipeInstructions = (id) => (dispatch) => {
  dispatch({ type: CONST.FETCH_INSTRUCTIONS_PENDING });
  axios
    .get(
      `https://api.spoonacular.com/recipes/${id}/analyzedInstructions?apiKey=${CONST.API_KEY}`
    )
    .then((response) => {
      dispatch({
        type: CONST.FETCH_INSTRUCTIONS_SUCCESS,
        instructions: response.data.length ? response.data[0].steps : [],
      });
    })
    .catch((error) => {
      dispatch({
        type: CONST.FETCH_INSTRUCTIONS_FAILED,
        error: error.message,
      });
    });
};

export const fetchSimilarRecipes = (storage, id) => (dispatch) => {
  dispatch({ type: CONST.FETCH_SIMILAR_RECIPES_PENDING });
  axios
    .get(
      ` https://api.spoonacular.com/recipes/${id}/similar?apiKey=${CONST.API_KEY}`
    )
    .then((response) => {
      const similarRecipeIds = response.data
        .map((recipe) => recipe.id)
        .slice(0, 4);
      dispatch({
        type: CONST.FETCH_SIMILAR_RECIPES_SUCCESS,
        recipeIds: similarRecipeIds,
      });
      const missingIds = extractMissingIds(storage, similarRecipeIds);
      if (missingIds) {
        dispatch(fetchRecipes(missingIds));
      }
    })
    .catch((error) => {
      dispatch({
        type: CONST.FETCH_SIMILAR_RECIPES_FAILED,
        error: error.message,
      });
    });
};
