import { combineReducers } from "redux";
import searchReducer from "../redux/reducers/searchReducer";
import recipeReducer from "../redux/reducers/recipeReducer";
import instructionsReducer from "../redux/reducers/instructionsReducer";
import recipeStorageReducer from "../redux/reducers/recipeStorageReducer";
import similarRecipesReducer from "../redux/reducers/similarRecipesReducer";

const rootReducer = combineReducers({
  searchReducer,
  recipeReducer,
  instructionsReducer,
  recipeStorageReducer,
  similarRecipesReducer,
});

export default rootReducer;
