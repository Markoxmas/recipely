export default function extractMissingIds(storage, ids) {
  const storageIds = storage.map((item) => item.id);
  //if the storage doesn't include the id, return it.
  return ids.filter((id) => !storageIds.includes(id)).join(",");
}
