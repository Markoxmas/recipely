import CONST from "../../../redux/constants";
import getRecipeInstructions from "../getRecipeInstructions";
import getRecipes from "../getRecipes";
import getSimilarRecipes from "../getSimilarRecipes";
import extractMissingIds from "../../extractMissingIds";

export const fetchRecipes = (ids) => (dispatch) => {
  const shouldSucceed = true;
  dispatch({ type: CONST.FETCH_RECIPES_PENDING });
  if (shouldSucceed) {
    setTimeout(() => {
      dispatch({
        type: CONST.FETCH_RECIPES_SUCCESS,
        recipes: getRecipes(ids),
      });
    }, 1000);
  } else {
    setTimeout(() => {
      dispatch({
        type: CONST.FETCH_RECIPES_FAILED,
        error: "Fetching recipes bulk failed!",
      });
    }, 1000);
  }
};

export const fetchRecipeInstructions = (id) => (dispatch) => {
  const shouldSucceed = true;
  dispatch({ type: CONST.FETCH_INSTRUCTIONS_PENDING });
  if (shouldSucceed) {
    setTimeout(() => {
      dispatch({
        type: CONST.FETCH_INSTRUCTIONS_SUCCESS,
        instructions: getRecipeInstructions()[0].steps,
      });
    }, 1000);
  } else {
    setTimeout(() => {
      dispatch({
        type: CONST.FETCH_RECIPE_FAILED,
        error: "Fetching the recipe instructions failed!",
      });
    }, 1000);
  }
};

export const fetchSimilarRecipes = (storage, id) => (dispatch) => {
  const shouldSucceed = true;
  dispatch({ type: CONST.FETCH_SIMILAR_RECIPES_PENDING });
  if (shouldSucceed) {
    const similarRecipeIds = getSimilarRecipes().map((recipe) => recipe.id);
    setTimeout(() => {
      dispatch({
        type: CONST.FETCH_SIMILAR_RECIPES_SUCCESS,
        recipeIds: similarRecipeIds,
      });
    }, 1000);

    const missingIds = extractMissingIds(storage, similarRecipeIds);
    if (missingIds) {
      setTimeout(() => {
        dispatch(fetchRecipes(similarRecipeIds.join(",")));
      }, 1000);
    }
  } else {
    setTimeout(() => {
      dispatch({
        type: CONST.FETCH_SIMILAR_RECIPES_FAILED,
        error: "Fetching the similar recipes failed!",
      });
    }, 1000);
  }
};
