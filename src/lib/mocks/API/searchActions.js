import CONST from "../../../redux/constants";
import getRecipeItems from "../getRecipeItems";

export const fetchRecipesAutocomplete = (searchField) => (dispatch) => {
  const shouldSucceed = true;
  dispatch({ type: CONST.FETCH_RECIPES_AUTO_PENDING });
  if (shouldSucceed) {
    setTimeout(() => {
      dispatch({
        type: CONST.FETCH_RECIPES_AUTO_SUCCESS,
        suggestions: getRecipeItems(),
      });
    }, 1000);
  } else {
    setTimeout(() => {
      dispatch({
        type: CONST.FETCH_RECIPES_AUTO_FAILED,
        error: "Autocomplete fetching failed!",
      });
    }, 1000);
  }
};
