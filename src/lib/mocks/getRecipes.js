import getFullRecipe from "./getFullRecipe";

export default function getRecipes(ids) {
  return ids.split(",").map((recipe) => getFullRecipe(parseInt(recipe)));
}
