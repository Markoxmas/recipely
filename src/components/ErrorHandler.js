import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { makeStyles } from "@material-ui/core/styles";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
}));

export default function ErrorHandler() {
  const classes = useStyles();
  const { error: instructions } = useSelector(
    (state) => state.instructionsReducer
  );
  const { error: storage } = useSelector((state) => state.recipeStorageReducer);
  const { error: search } = useSelector((state) => state.searchReducer);
  const { error: similar } = useSelector(
    (state) => state.similarRecipesReducer
  );
  const errors = [instructions, storage, search, similar];
  const [open, setOpen] = React.useState(false);

  useEffect(() => {
    if (errors.filter((error) => error !== "").length) {
      setOpen(true);
    }
  }, [instructions, storage, search, similar]);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          {errors.filter((error) => error !== "").join(" ")}
        </Alert>
      </Snackbar>
    </div>
  );
}
