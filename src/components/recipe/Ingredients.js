import React from "react";
import Chip from "@material-ui/core/Chip";

export default function Ingredients({ ingredients }) {
  return (
    <>
      {ingredients &&
        ingredients.map((ingredient) => (
          <Chip
            label={ingredient.name}
            color="primary"
            style={{ margin: "3px" }}
          />
        ))}
    </>
  );
}
