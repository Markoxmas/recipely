import React from "react";
import Grid from "@material-ui/core/Grid";
import RecipeItem from "./RecipeItem";
import Container from "@material-ui/core/Container";

export default function RecipeList({ recipeIds, recipes, setRecipeId }) {
  return (
    <Container>
      <Grid container justifyContent="center">
        {recipeIds &&
          recipes
            .filter((recipe) => recipeIds.includes(recipe.id))
            .map((item) => (
              <Grid lg={3} md={4} xs={6} style={{ padding: "5px" }}>
                <RecipeItem item={item} setRecipeId={setRecipeId} />
              </Grid>
            ))}
      </Grid>
    </Container>
  );
}
