import React from "react";
import Ingredients from "./Ingredients";
import PreparationSteps from "./PreparationSteps";

export default function RecipeContent({
  classes,
  recipe,
  instructions,
  pending,
}) {
  return (
    <div className={classes.content}>
      <div className={classes.ingredients}>
        <img src={recipe.image} alt={recipe.title} width="auto" height="auto" />
        <div>
          <h1>Ingredients</h1>
          <Ingredients ingredients={recipe.extendedIngredients} />
        </div>
      </div>
      <PreparationSteps classes={classes} steps={instructions} />
    </div>
  );
}
