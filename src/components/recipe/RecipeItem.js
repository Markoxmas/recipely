import React from "react";
import { useDispatch } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";

const useStyles = makeStyles({
  root: {
    textAlign: "center",
  },
  media: {
    height: 140,
  },
  button: {
    margin: "auto",
  },
  link: {
    textDecoration: "none",
  },
});

export default function RecipeItem({ item, setRecipeId }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={item.image}
          title={item.title}
        />
        <CardContent>
          <Typography gutterBottom variant="h6" component="h2">
            {item.title}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <div className={classes.button}>
          <Link to="/recipe" className={classes.link}>
            <Button
              size="small"
              color="primary"
              onClick={() => dispatch(setRecipeId(item.id))}
            >
              Details
            </Button>
          </Link>
        </div>
      </CardActions>
    </Card>
  );
}
