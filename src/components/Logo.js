import React from "react";

export default function Logo() {
  return <img src="/logo.png" alt="logo" width="326px" height="71px" />;
}
