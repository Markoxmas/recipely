import React from "react";
import { useSelector } from "react-redux";
import Search from "../search/Search";
import RecipeList from "../recipe/RecipeList";
import { makeStyles } from "@material-ui/core/styles";
import Logo from "../Logo";
import { onSetRecipeId } from "../../redux/actions/recipeActions";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  search: {
    marginTop: "100px",
    marginBottom: "250px",
    background: "#E0E0E0",
  },
  suggestions: {
    position: "absolute",
    border: "1px solid black",
    borderRadius: "0 0 20px 20px",
    width: "210px",
    background: "#E0E0E0",
  },
  suggestionLoad: {
    position: "absolute",
    margin: "auto",
  },
}));

export default function Home() {
  const classes = useStyles();
  const { history } = useSelector((state) => state.recipeReducer);
  const { recipes } = useSelector((state) => state.recipeStorageReducer);
  return (
    <div className={classes.root}>
      <Logo />
      <Search classes={classes} />
      {history.length > 0 && <h2>Previous recipes </h2>}
      <RecipeList
        recipeIds={history}
        recipes={recipes}
        setRecipeId={onSetRecipeId}
      />
    </div>
  );
}
