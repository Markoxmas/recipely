import React from "react";
import { useSelector } from "react-redux";
import RecipeList from "../recipe/RecipeList";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { onSetRecipeId } from "../../redux/actions/recipeActions";
import Logo from "../Logo";
import RecipeContent from "../recipe/RecipeContent";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  link: {
    textDecoration: "none",
    color: "white",
  },
  backButton: {
    margin: "40px",
  },
  content: {},
  ingredients: {
    display: "flex",
    textAlign: "center",
  },
  steps: {},
}));

export default function Recipe() {
  const classes = useStyles();
  const { recipeId } = useSelector((state) => state.recipeReducer);
  const { recipes, pending: storagePending } = useSelector(
    (state) => state.recipeStorageReducer
  );
  const { instructions, pending: instructionsPending } = useSelector(
    (state) => state.instructionsReducer
  );
  const { similarIds, pending: similarPending } = useSelector(
    (state) => state.similarRecipesReducer
  );
  return (
    <div className={classes.root}>
      <Logo />

      <Button
        variant="contained"
        size="large"
        color="primary"
        className={classes.backButton}
      >
        <Link to="/" className={classes.link}>
          Back to search
        </Link>
      </Button>

      {recipeId &&
        recipes &&
        recipes
          .filter((recipe) => recipe.id === recipeId)
          .map((recipe) => (
            <div>
              {storagePending && similarPending && instructionsPending ? (
                <CircularProgress />
              ) : (
                <>
                  <RecipeContent
                    classes={classes}
                    recipe={recipe}
                    instructions={instructions}
                    pending={instructionsPending}
                  />
                  <h1 style={{ textAlign: "center" }}>Similar recipes</h1>
                  <RecipeList
                    recipeIds={similarIds}
                    recipes={recipes}
                    setRecipeId={onSetRecipeId}
                  />
                </>
              )}
            </div>
          ))}
    </div>
  );
}
