import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";

export default function SuggestionLoad({ classes }) {
  return (
    <List dense={false}>
      <ListItem>
        <div className={classes.suggestionLoad}>
          <CircularProgress />
        </div>
      </ListItem>
    </List>
  );
}
