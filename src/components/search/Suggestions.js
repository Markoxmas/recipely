import React from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { Link } from "react-router-dom";

export default function Suggestions({ classes, suggestions, loadRecipy }) {
  return (
    <div className={classes.suggestions}>
      <List dense={false}>
        {suggestions.map((item) => (
          <ListItem>
            <Link
              to="/recipe"
              style={{
                textDecoration: "none",
                color: "black",
              }}
            >
              <ListItemText
                primary={item.title}
                onClick={() => loadRecipy(item.id)}
              />
            </Link>
          </ListItem>
        ))}
      </List>
    </div>
  );
}
