import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { onUpdateSearchField } from "../../redux/actions/searchActions";
import extractMissingIds from "../../lib/extractMissingIds";
import SuggestionLoad from "./SuggestionLoad";
import Suggestions from "./Suggestions";
import { onSetRecipeId } from "../../redux/actions/recipeActions";
import SearchField from "./SearchField";
import { fetchRecipesAutocomplete } from "../../redux/actions/searchActions";
import {
  fetchRecipes,
  fetchRecipeInstructions,
  fetchSimilarRecipes,
} from "../../redux/actions/recipeActions";

export default function Search({ classes }) {
  const dispatch = useDispatch();
  const { searchField, pending, suggestions, error } = useSelector(
    (state) => state.searchReducer
  );
  const { recipes } = useSelector((state) => state.recipeStorageReducer);

  const handleChange = (event) => {
    dispatch(onUpdateSearchField(event));
    dispatch(fetchRecipesAutocomplete(searchField));
  };

  const loadRecipy = (id) => {
    const missingIds = extractMissingIds(recipes, [id]);
    if (missingIds) {
      dispatch(fetchRecipes(id.toString()));
    }
    dispatch(fetchRecipeInstructions(id));
    dispatch(fetchSimilarRecipes(recipes, id));
    dispatch(onSetRecipeId(id));
  };
  return (
    <form className={classes.search} noValidate autoComplete="off">
      <SearchField handleChange={handleChange} searchField={searchField} />
      {pending && <SuggestionLoad classes={classes} />}
      {searchField && suggestions.length > 0 && (
        <Suggestions
          classes={classes}
          suggestions={suggestions}
          loadRecipy={loadRecipy}
        />
      )}
    </form>
  );
}
