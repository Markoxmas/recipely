import React from "react";
import TextField from "@material-ui/core/TextField";

export default function SearchField({ handleChange, searchField }) {
  return (
    <TextField
      id="outlined-basic"
      label="Search"
      variant="outlined"
      value={searchField}
      onChange={(event) => handleChange(event)}
    />
  );
}
