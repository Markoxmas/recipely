import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Home from "./components/pages/Home";
import Recipe from "./components/pages/Recipe";
import { BrowserRouter as Router, Route } from "react-router-dom";
import ErrorHandler from "./components/ErrorHandler";

function App() {
  return (
    <Router>
      <CssBaseline />
      <Container maxWidth="md">
        <ErrorHandler />
        <Typography component="div" />
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/recipe">
          <Recipe />
        </Route>
      </Container>
    </Router>
  );
}

export default App;
