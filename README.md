Recipely is an app that lets you search recipies. It also shows you the ingredients that are used in those recipies and explains how to prepare them.

Frontend library: React  
State management: Redux  
API calling: Axios, Redux-thunk  
UI Framework: Material-ui  
Routing: React-router-dom  
API: Spoonacular

Folder structure:  
components -> contains pages Home and Recipe + other components  
lib -> contains folder with mocks that helped me develop the app before I attached it to the real API (not to burn the credits while testing).  
redux -> contains action creators, reducers and other files needed to configure redux

In order to run the project locally, simply clone the repository, npm install modules and npm start the app.

The app is also hosted on firebase at https://recipely-190c7.web.app/
